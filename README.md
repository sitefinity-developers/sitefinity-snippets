# Sitefinity Project Snippets

This project is a container for one-off files and short code snippets that won't work as standalone projects.

Use the GitLab [Snippets](https://gitlab.engr.illinois.edu/sitefinity-developers/sitefinity-snippets/snippets) function
to add new code samples.

If your code sample can stand on its own, consider creating a new GitLab project showcasing that solution.